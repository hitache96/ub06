package de.unistuttgart.iste.rss.oo.hamstersimulator.sheet06.homework.exercise2;

import de.unistuttgart.iste.rss.oo.hamstersimulator.external.model.SimpleHamsterGame;


/**
 * Beschreiben Sie hier die Klasse HomeworkHamster.
 * 
 * @author Moneeb Adel, st168568@stud.uni-stuttgart.de, 3479230
 * @author Abdulsamad Abdulfattah, st159779@stud.uni-stuttgart.de, 3332720
 * @version (3V)
 */
public class AmazingPauleHamsterGame extends SimpleHamsterGame {

    /**
     * Creates a new AmazingPauleHamsterGame
     * do not modify
     */
    public AmazingPauleHamsterGame() {
        this.loadTerritoryFromResourceFile("/territories/AmazingPauleTerritory.ter");
        this.displayInNewGameWindow();
        game.startGame();
    }
    
    /**
     * Ignore this method. Put your code in passTheMaze()
     */
    @Override
    protected void run() {
        passTheMaze();
    }
    
    /**
	 * Has Paule walk through the labyrinth till he finds a grain.
	 * 
	 * Takes a look at the Paule object and checks for a grain in his mouth. 
	 * If Paule has a grain in his mouth the labyrinth was solved, else he will walk along the wall to his right until
	 * he arrives on the field which contains a grain, at which point he picks it up, leading to the same result as the first case.
	 */
	/*@
	 @	requires paule.mouthEmpty() == true ; Paules mouth has to be empty at the beginning
	 @ 	requires territory.getTotalGrainCount >= 1 ; The number of grains has to be 1 or bigger  
	 @	requires territory.isLocationInTerritory(paule.getLocation().getColumn(), paule.getLocation().getRow()) != false; Paules location has to lie in the territory
	 @	requires the territory to be initialized  
	 @
	 @ 	ensures paule.getLocation() is the Location of the grain 
	 @	ensures paule.mouthEmpty() != true; Paule has to have a grain in his mouth, confirming he finished the task 
	 */

	void passTheMaze() {
		
		/*@
		 @	loop_invariant Paule has 1 grain in his mouth 
		 */
		/**
		 * We check whether or not Paule has a grain in his mouth. If he has a grain in his mouth the task is considered finished.
		 * If Paules mouth is empty he walks along the wall to his right. 
		 * Paule will walk in the direction hes facing, while checking along every step of the way whether or not he can turn right.
		 * He will follow whichever path has him stick to the wall on his right, even if that behaviour leads him to walk into a deadend.
		 * 
		 */
		while(paule.mouthEmpty())
			if(paule.grainAvailable()) {
				paule.pickGrain();
				
			}else {
				if(paule.frontIsClear()) {
					paule.turnLeft();
					paule.turnLeft();
					paule.turnLeft();
					if(paule.frontIsClear()) {
						paule.move();
					}else {
						paule.turnLeft();
						paule.move();
					}
					
				}else {
					paule.turnLeft();
					paule.turnLeft();
					paule.turnLeft();
					if(paule.frontIsClear()) {
						paule.move();
						
					}else if(paule.frontIsClear() != true) {
						paule.turnLeft();
						paule.turnLeft();
						if(paule.frontIsClear() != true) {
							paule.turnLeft();
							paule.move();
							
						}else { 
							paule.move();
						}
					}
				}
			}
									
					
								
									
								
							
						
					
			
		}
    
}