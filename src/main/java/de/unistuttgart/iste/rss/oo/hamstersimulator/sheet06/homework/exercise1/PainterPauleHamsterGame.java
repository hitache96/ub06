package de.unistuttgart.iste.rss.oo.hamstersimulator.sheet06.homework.exercise1;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import de.unistuttgart.iste.rss.oo.hamstersimulator.external.model.SimpleHamsterGame;

/**
 * Diese Klasse l�sst Paule eine Spierale auf jedem quadratischen Territory mit
 * den K�rner zeichnen k�nnen.
 * 
 * @author Moneeb Adel, st168568@stud.uni-stuttgart.de, 3479230
 * @author Abdulsamad Abdulfattah, st159779@stud.uni-stuttgart.de, 3332720
 * @version (03V.)
 */
public class PainterPauleHamsterGame extends SimpleHamsterGame {

	/**
	 * Creates a new PainterPauleHamsterGame do not modify
	 */
	public PainterPauleHamsterGame() {
		this.loadTerritoryFromResourceFile("/territories/PainterPauleTerritory.ter");
		this.displayInNewGameWindow();
		game.startGame();
	}
	public void pickOrMove () {
		
		if (paule.frontIsClear()&& paule.mouthEmpty() && paule.grainAvailable()) {
			paule.pickGrain();
			paule.move();
		}
		
		
		
	}
	
	public static void main (String[] args) {
		List <Integer> steps = new LinkedList <Integer>();
		Stack sets = new Stack();
		Queue <Integer> teps = new LinkedList<Integer>();
		sets.add(5);
		sets.push(6);
		sets.push(8);
		
		steps.add(3);
		steps.add(2);
		teps.add(1);
		teps.add(0);
		//steps.remove(0);
		System.out.println(steps);
		System.out.println(sets);
		System.out.println(teps);
	}

	/**
	 * Run Method which calls some another Methods and lets them interacting with an
	 * Algorithm to paint a Spiral
	 * 
	 */
	@Override
	protected void run() {
		

		measureSquaresSide();
		while (!paule.mouthEmpty()) {
			if (turnCounter < 2) {
				for (int i = 0; i < 2; i++) {
					smartTurnLeft();
					moveNTimesPaint(stepsCounter);

				}
			} else {
				stepsCounter--;
				stepsCounter--;
				turnCounter = 0;
			}
		}
		

	}


	// an Integer to recognize how long is the Squares Side 
	int stepsCounter = 0;
	// an Integer to recognize how many times did Paule turn Left
	int turnCounter = 0;

	/*
	 * Paule turns Left and saves how many times he turned to the Left
	 * 
	 * @requires paule.isInitialized;
	 * 
	 * @requires is.Initialized;
	 * 
	 * @ensures Paule turned to the Left
	 * 
	 * @ensures old(turnCounter) < turnCounter
	 */
	void smartTurnLeft() {
		paule.turnLeft();
		turnCounter++;
	}

	/*
	 * The Method lets Paule move steps Times
	 * 
	 * @requires !paule.mouthEmpty;
	 * 
	 * @requires paule.frontIsClear;
	 * 
	 * @ensures Paule moved steps Times
	 * 
	 * @ensures there less Corns in Paules Mouth
	 */
	void moveNTimesPaint(int steps) {
		/*
		 * @loop_inviant Paule visited steps Tiles
		 * 
		 * @decreasing steps-i
		 */
		for (int i = 0; i < steps; i++) {
			paule.putGrain();
			if (!paule.mouthEmpty()) {
				paule.move();
			}
		}
	}

	/**
	 * This Method is has the same Code in the run Method It lets Paule to paint a
	 * Spiral in any Square
	 * 
	 * @requires Paule starts at the Begin of the Squares Side, not in the middle or
	 * anything like that
	 * 
	 * @requires Paule looks in a direction that lets him makes circles in
	 * anticlockwise rotation
	 * 
	 * @requires paule.isInitialized;
	 * 
	 * @requires isInitialized;
	 * 
	 * @ensures Paule painted a Spiral
	 * 
	 * @ensures Paule has no Corns in his Mouth
	 */
	public void paintSpiral() {
		measureSquaresSide();
		while (!paule.mouthEmpty()) {
			// get ready to minimize the go in the Spiral
			if (turnCounter < 2) {
				for (int i = 0; i < 2; i++) {
					smartTurnLeft();
					moveNTimesPaint(stepsCounter);

				}
				// minimize the Sizes of the Squares Side to rich the Spirals Form
			} else {
				stepsCounter--;
				stepsCounter--;
				turnCounter = 0;
			}
		}
	}

	/*
	 * get the Size of the Sequares Side
	 * 
	 * @requires Paule is at the Begin of the Squares Side
	 * 
	 * @requires paule.isInitialzed;
	 * 
	 * @requires stepsLengthCounter=0;
	 * 
	 * @ensures stepsLengthCounter > 0
	 * 
	 * @ensures Paule moved (stepsLengthCounter) times
	 */
	private void measureSquaresSide() {
		while (paule.frontIsClear()) {
			paule.putGrain();
			paule.move();
			stepsCounter++;
		}
	}
	
}
